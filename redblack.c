#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>

struct list {
	int a;
	char color;
	struct list* parent;
	struct list* left;
	struct list* right;
};

struct list *grandparent(struct list *n){
	if((n!=NULL) && (n->parent != NULL))
		return n->parent->parent;
	else
		return NULL;
}

struct list *uncle(struct list *n){
	struct list *g=grantparent(n);
	if(g == NULL)
		return NULL;
	if (n->parebt == g->left)
		return g->right;
	else
		return g->left;

}

void r_left(struct list *n){
	struct list *pivot = n->right;
	pivot->parent=n->parent;
	if (n->parent != NULL){
		if (n->parent->left == n)
			n->parent->left = pivot;
		else
			n->parent->right = pivot;
	}

	n->right = pivot->left;
	if (pivot->left != NULL)
		pivot->left->parent = n;

	n->parent = pivot;
	pivot->left=n;
}

void r_right(struct list *n){
	struct list *pivot = n->left;
	pivot->parent = n->parent;
	if (n->parent != NULL)
		if(n->parent->left == n)
			n->parent->left=pivot;
		else
			n->parent->right=pivot;
	}

	n->left=pivot->right;
	if(pivot->right!= NULL)
		pivot->right->parent=n;
	n->parent=pivot;
	pivot->right=n;
}

void case_1(struct list *n){
	if(n->parent == NULL)
		n->color = 'b';
	else
		case_2(n);
}

void case_2(struct list *n ){
	if (n->parent->color == 'b')
		return;
	else
		case_3(n);
}

void case_3(struct list *n){
	struct node *u = uncle(n), *g;

	if ((u != NULL) && (u->color == 'r')&&(n->parent->color == 'r')){
		n->parent->color = 'b';
		u->color = 'b';
		g = grantparent (n);
		g->color = 'r';
		case_1(g);
	}
	else
		case_4(n);
}

void case_4(struct list *n){
	struct list *g = grandparent(n);

	if ((n == n->parent->right)&&(n->parent == g->left)){
		r_left(n->parent);
		n=n->left;
	}
	else if ((n == n->parent->left)&&(n->parent == g->right)){
		r_right(n->parent);
		n=n->right;
	}
	case_5(n);
}

void case_5(struct list *n){
	struct list *g=grandparent(n);
	n->parent->color= 'b';
	g->color = 'r';
	if ((n==n->parent->left)&&(n->parent==g->left)){
		r_right(g);
	}else
		r_left(g);
}

int main()
{
	int i, a, b, *c;

	printf("Vvedite kol-vo chisel: ");
	scanf("%d", &b);

	c=malloc(sizeof(int)*b);

	for (i=0; i<b; i++){
		c[i]=rand()%100;
	}

	struct list *root;
	root=malloc(sizeof (struct list));
	root->a=c[0];
	root->color='b';
	root->left=NULL;
	root->right=NULL;
	root->parent=NULL;

	struct list *tmp;

	for(i=1; i<b; i++){
		tmp=root;
		struct list *node;
		node=malloc(sizeof(struct list));
		node->a=c[i];
		node->color='r';
		node->left=NULL;
		node->right=NULL;
		while(1){
			if(node->a==tmp->a) break;
			if(node->a < tmp->a ){
				if(tmp->left != NULL)
					tmp=tmp->left;
				else{
					tmp->left=node;
					tmp->left->parent=tmp;
					tmp=tmp->left;
					break;
				}
			}
			else{
				if(tmp->right != NULL)
					tmp=tmp->right;
				else{
					tmp->right=node;
					tmp->right->parent=tmp;
					tmp=tmp->right;
					break;
				}
			}
		}
		case_1(tmp);
		while(1){
			if (root->parent != NULL)
				root=root->parent;
			else
				break;
		}
	}

}
